# -*- coding: utf-8 -*-
def index():
        proveedores=db().select(db.t_proveedores.ALL)
        form = SQLFORM.factory(
        Field( 'nombre', requires=IS_IN_DB(db,'t_proveedores.id','%(f_nombre)s')))
        if form.process().accepted:
            response.flash = ' aceptado/a'
            proveedores=db(db.t_proveedores.f_nombre==form.vars.nombre).select(db.t_proveedores.ALL)
        elif form.errors:
                 response.flash = 'form has errors'
        return dict(form=form,proveedores=proveedores)
    
@auth.requires_login()
def proveedores_manage():
    form=crud.create(db.t_proveedores)
    proveedores=db().select(db.t_proveedores.ALL)
    return dict(proveedores=proveedores,form=form)

@auth.requires_login()
def crear_proveedor():
    id_proveedor=request.args(0)
    form = crud.create(db.t_proveedores)
    return dict(form=form)

@auth.requires_login()
def editar_proveedor():
	id_proveedor=request.args(0)
	form = crud.update(db.t_proveedores,id_proveedor)
	return dict(form=form)

@auth.requires_login()
def ver_proveedor():
	id_proveedor=request.args(0)
	form = crud.read(db.t_proveedores,id_proveedor)
	return dict(form=form)

@auth.requires_login()
def eliminar_proveedor():
	id_proveedor=request.args(0)
	form = crud.delete(db.t_proveedores,id_proveedor)
	return dict(form=form)
