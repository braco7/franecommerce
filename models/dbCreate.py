# -*- coding: utf-8 -*-
def button_icon(texto,icono,controlador,funcion,args=[]):
    return SPAN(A(texto,_href=URL(c=controlador,f=funcion,args=args),_class=icono))

db.define_table('t_tipos',
                Field('f_codigo',type='string'),
                Field('f_descripcion',type='string'),
                auth.signature,
                format='%(f_descripcion)s',
                migrate=settings.migrate)

db.define_table('t_tipos_archive',db.t_tipos,Field('current_record','reference t_tipos',readable=False,writable=False))


db.define_table('t_marcas',
                Field('f_codigo',type='string'),
                Field('f_marca',type='string'),
                auth.signature,
                format='%(f_marca)s',
                migrate=settings.migrate)

db.define_table('t_marcas_archive',db.t_marcas,Field('current_record','reference t_marcas',readable=False,writable=False))

db.define_table('products',
                Field('productName','string',unique=True,requires=IS_LENGTH(40,6)),
                Field('productDescription','string',requires=IS_LENGTH(450,6)),
                Field('productImage','upload',unique = True,uploadseparate=True,autodelete=True,requires=([IS_IMAGE(extensions=('jpeg', 'png')),IS_LENGTH(3145728, 1024)])),
                Field('productImage2','upload',unique = True,uploadseparate=True,autodelete=True,requires=IS_EMPTY_OR([IS_IMAGE(extensions=('jpeg', 'png')),IS_LENGTH(3145728, 1024)])),
                Field('productPrice','float',requires=[IS_DECIMAL_IN_RANGE(0.01, 1000.00,dot='.'),IS_NOT_EMPTY()]),
                Field('productPricemayor','float',requires=[IS_DECIMAL_IN_RANGE(0.01, 1000.00,dot='.'),]),
                Field('productQuantity','integer',requires=IS_INT_IN_RANGE(0, 10000)),
                Field('productSize','reference t_marcas'),
                Field('productType','string'),#'reference t_tipos'),#requires=IS_IN_SET(["Crema de Leche"])),
                Field('productAvgRating','float',requires=IS_DECIMAL_IN_RANGE(0.00, 5.00, dot='.'))
               )
db.define_table('products_archive',db.products,Field('current_record','reference products',readable=False,writable=False))


db.define_table('product_reviews',
                 Field('reviewsTitle','text',requires=IS_LENGTH(64,4)),
                 Field('reviewsRating','integer',requires=IS_IN_SET(range(1, 6))),
                 Field('reviewsText','text',requires=IS_LENGTH(256,6)),
                 Field('productID','reference products','integer',requires=IS_NOT_EMPTY()),
                 auth.signature
               )
db.define_table('product_reviews_archive',db.product_reviews,Field('current_record','reference product_reviews',readable=False,writable=False))

db.define_table('t_proveedores',
                Field('f_nombre',type='string'),
                Field('f_direccion',type='string'),
                Field('f_telefono',type='integer'),
                Field('f_personaContacto',type='string'),
                auth.signature,
                format='%(f_nombre)s',
                migrate=settings.migrate)
db.define_table('t_proveedores_archive',db.t_proveedores,Field('current_record','reference t_proveedores',readable=False,writable=False))

db.define_table('t_contenidos',
                Field('f_seccion',type='string',requires=IS_IN_SET(["SERVICIO AL CLIENTE","INFORMACION UTIL","ACERCA DE"])),
                Field('f_nombre',type='string'),
                Field('f_descripcion',type='string'),
                Field('f_contenido',type='text'),
                auth.signature,
                format='%(f_nombre)s',
                migrate=settings.migrate)
db.define_table('t_contenidos_archive',db.t_contenidos,Field('current_record','reference t_contenidos',readable=False,writable=False))




#To clear tables if needed
#db.products.truncate()
#db.product_reviews.truncate()
#db.auth_user.truncate()
#db.auth_group.truncate()
#db.auth_membership.truncate()
