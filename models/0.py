# -*- coding: utf-8 -*-
from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Inmobiliaria1'
settings.subtitle = 'powered by web2py'
settings.author = 'guasos.dev'
settings.author_email = 'you@example.com'
settings.keywords = None
settings.description = None
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'dc3d878e-5643-44cf-b3b0-f8dde29a4d1f'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = None
settings.login_method = 'local'
settings.login_config = None
settings.plugins = []
